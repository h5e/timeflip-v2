# TimeFlip node

Proof of concept to subscribe to [TimeFlip v2](https://timeflip.io/) ble events using Node and [Noble](https://www.npmjs.com/package/@abandonware/noble).

It connects to the first TimeFLip it finds and subscribes to its data events. I might make this more interesting later but don't count on it.

## Useful links
* [TimeFlip v2 ble spec](https://github.com/DI-GROUP/TimeFlip.Docs/blob/master/Hardware/TimeFlip%20BLE%20protocol%20ver4_02.06.2020.md)
* [Noble](https://github.com/abandonware/noble/blob/master/README.md)
