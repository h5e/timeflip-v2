import noble, {Characteristic, Peripheral} from '@abandonware/noble';

/*
  Documentation
  =============

  TimeFlip: https://github.com/DI-GROUP/TimeFlip.Docs/blob/master/Hardware/TimeFlip%20BLE%20protocol%20ver4_02.06.2020.md
  Noble: https://github.com/abandonware/noble/blob/master/README.md
 */

const timeFlipServiceUUID = 'f1196f5071a411e6bdf40800200c9a66';
const timeFlipEventCharacteristicUUID = 'f1196f5171a411e6bdf40800200c9a66'

const previouslyDiscoveredPeripherals: string[] = [];
noble.on('discover', async (peripheral) => {
  // duplicates are not correctly filtered by startScanningAsync (it seems like devices with no localName are not registered)
  if (previouslyDiscoveredPeripherals.includes(peripheral.uuid)) {
    return;
  }
  previouslyDiscoveredPeripherals.push(peripheral.uuid);

  if (!peripheral.advertisement.localName) {
    return;
  }

  console.log(`Discovered: ${peripheral.uuid} ${peripheral.advertisement.localName}`);
  await peripheral.connectAsync();
  if (await isTimeFlip(peripheral)) {
    await noble.stopScanningAsync(); // just stop at the first found TimeFlip for now, eh.
    try {
      await registerTimeFlip(peripheral);
    } catch (e) {
      console.warn('error registering TimeFlip', e);
      await peripheral.disconnectAsync();
    }
    return;
  }

  await peripheral.disconnectAsync();
});

const isTimeFlip = async (connectedPeripheral: Peripheral) => {
  try {
    await connectedPeripheral.discoverSomeServicesAndCharacteristicsAsync([timeFlipServiceUUID], []);
    return true;
  } catch (e) {
    return false;
  }
}

// keeps global references to properly unsubscribe on shutdown
let timeFlip: Peripheral;
let timeFlipEventCharacteristic: Characteristic;

const registerTimeFlip = async (connectedTimeFlip: Peripheral) => {
  if (timeFlip || timeFlipEventCharacteristic) {
    throw new Error('Already a timeFlip connected');
  }

  const characteristic = await getTimeFlipEventCharacteristic(connectedTimeFlip);
  if (!characteristic) {
    throw new Error('Cannot find timeFlipEventCharacteristic, are you sure this is a TimeFlip?');
  }

  timeFlip = connectedTimeFlip;
  timeFlipEventCharacteristic = characteristic;


  await characteristic.notifyAsync(true);
  characteristic.on('data', (data, isNotification) => {
    console.log('Data:', isNotification, data.toString());
  });

  console.log('Subscribed to TimeFlip events');
}

const getTimeFlipEventCharacteristic = async (timeFlip: Peripheral) => {
  const sericesAndCharacteristics = await timeFlip.discoverSomeServicesAndCharacteristicsAsync([], [timeFlipEventCharacteristicUUID]);
  return sericesAndCharacteristics.characteristics.find(c => c.uuid === timeFlipEventCharacteristicUUID);
}

const main = async () => {
  await poweredOn();

  // if given serviceUUIDs it won't find any (on macOS), maybe it needs a different format?
  await noble.startScanningAsync([], false);
}


const poweredOn = async () => {
  await new Promise((resolve) => {
    noble.on('stateChange', (state) => {
      console.log('Noble stateChange', state);
      if (state === 'poweredOn') {
        resolve(state);
      }
    })
  })
}

main();

process.on('SIGINT', function () {
  console.log('Caught interrupt signal');
  cleanup().then(() => process.exit());
});

process.on('SIGQUIT', function () {
  console.log('Caught interrupt signal');
  cleanup().then(() => process.exit());
});

process.on('SIGTERM', function () {
  console.log('Caught interrupt signal');
  cleanup().then(() => process.exit());
});

const cleanup = async() => {
  await noble.stopScanningAsync();
  await timeFlipEventCharacteristic?.notifyAsync(false);
  await timeFlip?.disconnectAsync();
}
